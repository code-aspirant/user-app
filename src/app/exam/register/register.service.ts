import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpService } from 'src/app/http.service';
import { CommonConstants } from '../../shared/commonConstants';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(public http:HttpService) { }

  saveProfile(form: NgForm) {
    return this.http.call('POST', CommonConstants.APIUrl.saveProfile, form);
  }

}
