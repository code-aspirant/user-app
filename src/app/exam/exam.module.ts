import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { ExamRoutingModule } from './exam-routing.module';
import { RegisterComponent } from './register/register.component';
import { SearchPipe } from '../pipes/search.pipe';

// import { MatCardModule, MatInputModule, MatButtonModule } from '@angular/material';

@NgModule({
  declarations: [RegisterComponent,SearchPipe],
  imports: [
    CommonModule,
    ExamRoutingModule,
    FormsModule
  //  RouterModule,MatCardModule,MatInputModule,MatButtonModule
  ],providers:[SearchPipe]
})
export class ExamModule {
  constructor() { console.log('exam module loaded') }
}
