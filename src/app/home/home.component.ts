import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public user: any;

  constructor() { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('userDetails'));
  }


}
