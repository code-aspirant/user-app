import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppUser, users } from '../model/appuser.model';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
//import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _loginservice: LoginService, public router: Router) { } 

  ngOnInit() { 
    // history.pushState(null,document.title,location.href) 
    // window.addEventListener('popstate',(event)=>{
    //   history.pushState(null,document.title,location.href) 
    // });
  }

  public customerObj: any;
  loginUser(loginUser: NgForm) {
    if (!(loginUser.value.email === '' && loginUser.value.password === '')) {
      this._loginservice.getCustomerData().subscribe((response) => {
        this.customerObj = JSON.parse(JSON.stringify(response));
        let result = this.customerObj.find(customer => customer.emailid === loginUser.controls['email'].value && customer.password === loginUser.controls['password'].value);
         
        sessionStorage.setItem('userDetails', JSON.stringify(result));
        result ? this.router.navigate(['profile']) : alert('invalid credentials');
      });
    } else {
      alert('provide login details')
    }
  }

}
