import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';
import { CommonConstants } from '../shared/commonConstants';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(public http:HttpService) { }

  getCustomerData() {
    return this.http.call('GET', CommonConstants.APIUrl.getCustomerData, '');
  }

}
