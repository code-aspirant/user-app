import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

//   transform(items: any[], value: string, prop?: string): any[] {
//     if (!items) return [];
//     if (!value) return items;

//     return items.filter(singleItem =>{
//      debugger 
//         singleItem[prop].toLowerCase().startsWith(value.toLowerCase())
//     });

// }

  transform(items: any[], searchText: string): any[] {
    if (!items) return [];
    if (!searchText) return items;
    searchText = searchText.toLowerCase();
    return items.filter(it => {
      return it.toLowerCase().includes(searchText);
    });
  }

}

