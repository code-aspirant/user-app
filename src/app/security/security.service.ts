import { Injectable } from '@angular/core';

import { AppUserAuth } from './appuser-auth';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor() { }

  securityobj: AppUserAuth = new AppUserAuth();

}
