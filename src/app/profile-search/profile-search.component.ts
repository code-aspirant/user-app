import { Component, OnInit } from '@angular/core';
import { ProfilesearchService } from './profilesearch.service';

@Component({
  selector: 'app-profile-search',
  templateUrl: './profile-search.component.html',
  styleUrls: ['./profile-search.component.scss']
})
export class ProfileSearchComponent implements OnInit {


  public user: any;
  public searchList:any;

  constructor(private searchservice:ProfilesearchService) { }

  ngOnInit() {
    this.user = JSON.parse(sessionStorage.getItem('userDetails'));
  }

  getData(search: any) {
    this.searchservice.fetchData(search).subscribe((response:any) => {              
        this.searchList = response.items      
    });
  }
}
