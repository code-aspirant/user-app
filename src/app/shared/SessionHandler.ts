export class SessionHandler {
    public static get() {
        const value = sessionStorage.getItem(name);

        if (value != null) {
            return value;
        } else {
            return '';
        }
    }

    public static set(name: string, value: string): void {
        sessionStorage.setItem(name, value);
    }
}