import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';
import { CommonConstants } from '../shared/commonConstants';


@Injectable({
  providedIn: 'root'
})
export class ProfilesearchService {

  constructor(public http:HttpService) { }


  fetchData(searchString: string) {
    return this.http.call('GET', CommonConstants.APIUrl.fetchProfile, searchString);
  }


}
