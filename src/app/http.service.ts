import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';




@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  public call(_type: any, _url: any, data: any) {
    if (_type == 'GET') {
      return this.http.get(_url+ data);
    } else {
      return this.http.post(_url, data);
    }

  }
}
