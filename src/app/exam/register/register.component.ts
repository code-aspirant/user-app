import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SearchPipe } from '../../pipes/search.pipe';
import { RegisterService } from './register.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  @ViewChild('registerForm') form: NgForm;


  searchText: any
  selected: any = [];

  technologies = [
    'J2EE',
    'Hibernate',
    'Hibeasdfdsa',
    'Hibedfghf',
    'JPA',
    'Selenium',
    'Angular',
    'ReactJs'
  ]
  constructor(public searchpipe: SearchPipe,private registerservice:RegisterService) { }

  ngOnInit() { }

  selectedTech(tech: string) {
    if (!this.selected.includes(tech)) { 
      this.selected.push(tech) ;
      console.log(this.form.controls['technologiesknown']);
    }
    this.searchText = ''
  }
  onSubmit(form:NgForm) {
    this.registerservice.saveProfile(form.value)
    //.subscribe((response)=>{console.log(response)})
  }
}
